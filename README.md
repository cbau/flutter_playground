# Flutter Playground

Welcome to the Flutter Playground! In this application you will find many small experiments featuring some Flutter curiosities and behaviors.

Open the [webapp](https://cbau.gitlab.io/flutter_playground) to see the experiments in action, select an option from the side menu to find an experiment you are interested in, and check the source code to see how these experiments were built.
