import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/info_card.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  static const routeName = 'AboutPage';

  @override
  Widget build(BuildContext context) => Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const InfoCard(
              title: 'About this app',
              description: '''
Welcome to the Flutter Playground! In this application you will find many small experiments featuring some Flutter curiosities and behaviors.
Select an option from the side menu to find an experiment you are interested in, and check the source code to see how these experiments were built.''',
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(AppService().baseUrl),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the source code'),
                  onPressed: followLink,
                ),
              ),
            ),
          ],
        ),
      );
}
