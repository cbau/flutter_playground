import 'package:flutter/material.dart';

class NavigationScaffold extends StatelessWidget {
  const NavigationScaffold({
    required this.destinations,
    this.appBar,
    this.body,
    this.onDestinationSelected,
    this.scaffoldKey,
    this.selectedIndex = 0,
    super.key,
  });

  final PreferredSizeWidget? appBar;
  final Widget? body;
  final List<NavigationDestination> destinations;
  final Key? scaffoldKey;
  final int selectedIndex;
  final ValueChanged<int>? onDestinationSelected;

  @override
  Widget build(BuildContext context) => LayoutBuilder(
        builder: (context, constrains) =>
            (destinations.length <= 5 && constrains.maxWidth < 600) ||
                    (destinations.length > 5 && constrains.maxWidth < 900)
                ? Scaffold(
                    appBar: appBar,
                    body: body,
                    bottomNavigationBar: destinations.length <= 5
                        ? NavigationBar(
                            selectedIndex: selectedIndex,
                            onDestinationSelected: onDestinationSelected,
                            destinations: destinations,
                          )
                        : null,
                    drawer: destinations.length > 5
                        ? NavigationDrawer(
                            selectedIndex: selectedIndex,
                            onDestinationSelected: (index) {
                              onDestinationSelected?.call(index);
                              Navigator.of(context).pop();
                            },
                            children: destinations
                                .map(
                                  (e) => NavigationDrawerDestination(
                                    icon: e.icon,
                                    label: Text(e.label),
                                    selectedIcon: e.selectedIcon,
                                  ),
                                )
                                .toList(),
                          )
                        : null,
                    key: scaffoldKey,
                  )
                : Row(
                    children: [
                      if (destinations.length > 5)
                        NavigationDrawer(
                          selectedIndex: selectedIndex,
                          onDestinationSelected: onDestinationSelected,
                          children: destinations
                              .map(
                                (e) => NavigationDrawerDestination(
                                  icon: e.icon,
                                  label: Text(e.label),
                                  selectedIcon: e.selectedIcon,
                                ),
                              )
                              .toList(),
                        )
                      else
                        NavigationRail(
                          destinations: [
                            ...destinations.map(
                              (e) => NavigationRailDestination(
                                icon: e.icon,
                                label: Text(e.label),
                                selectedIcon: e.selectedIcon,
                              ),
                            ),
                          ],
                          extended: constrains.maxWidth >= 900,
                          labelType: constrains.maxWidth >= 900
                              ? null
                              : NavigationRailLabelType.all,
                          onDestinationSelected: onDestinationSelected,
                          selectedIndex: selectedIndex,
                        ),
                      Expanded(
                        child: Scaffold(
                          appBar: appBar,
                          body: body,
                          key: scaffoldKey,
                        ),
                      ),
                    ],
                  ),
      );
}
