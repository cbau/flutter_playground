import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../app_logic/app_bloc.dart';
import '../common/info_card.dart';

class ResponsiveNavigationPage extends StatelessWidget {
  const ResponsiveNavigationPage({super.key});

  static const routeName = 'ResponsiveNavigationPage';

  @override
  Widget build(BuildContext context) => Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const InfoCard(
              title: 'Responsive navigation',
              description: '''
Yes! Even the navigation in this app is an experiment in this playground.
For this case, we created a NavigationScaffold class, not very customizable, but it creates automatically a NavigationDrawer that is openable from the AppBar on small screens, and fixed on the side in big screens.
Also, if there are less than six destinations in the navigation, it uses instead a NavigationBar for small screens and a NavigationRail for bigger screens, following the recommended navigation standards.''',
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(
                  '${AppService().baseUrl}/lib/presentation/navigation/',
                ),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the navigation folder'),
                  onPressed: followLink,
                ),
              ),
            ),
            if (MediaQuery.sizeOf(context).width < 900) ...[
              const SizedBox(
                height: 8,
              ),
              Center(
                child: ElevatedButton.icon(
                  icon: const Icon(Icons.menu),
                  label: const Text('Open the drawer'),
                  onPressed: () => BlocProvider.of<AppBloc>(context)
                      .scaffoldKey
                      .currentState
                      ?.openDrawer(),
                ),
              ),
            ],
          ],
        ),
      );
}
