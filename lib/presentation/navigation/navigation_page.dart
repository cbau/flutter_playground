import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../app_logic/app_bloc.dart';
import 'navigation_scaffold.dart';

class NavigationPage extends StatelessWidget {
  const NavigationPage({
    required this.navigationShell,
    super.key,
  });

  final StatefulNavigationShell navigationShell;

  static const _destinations = [
    NavigationDestination(
      icon: Icon(Icons.home_outlined),
      label: 'Home',
      selectedIcon: Icon(Icons.home),
    ),
    NavigationDestination(
      icon: Icon(Icons.widgets_outlined),
      label: 'Extended widgets',
      selectedIcon: Icon(Icons.widgets),
    ),
    NavigationDestination(
      icon: Icon(Icons.radio_button_checked_outlined),
      label: 'External pickers',
      selectedIcon: Icon(Icons.radio_button_checked),
    ),
    NavigationDestination(
      icon: Icon(Icons.group_work_outlined),
      label: 'Grouped list',
      selectedIcon: Icon(Icons.group_work),
    ),
    NavigationDestination(
      icon: Icon(Icons.open_with_outlined),
      label: 'Horizontal in vertical list',
      selectedIcon: Icon(Icons.open_with),
    ),
    NavigationDestination(
      icon: Icon(Icons.arrow_drop_down_outlined),
      label: 'Nested pickers',
      selectedIcon: Icon(Icons.arrow_drop_down),
    ),
    NavigationDestination(
      icon: Icon(Icons.dataset_outlined),
      label: 'Responsive form',
      selectedIcon: Icon(Icons.dataset),
    ),
    NavigationDestination(
      icon: Icon(Icons.menu_outlined),
      label: 'Responsive navigation',
      selectedIcon: Icon(Icons.menu),
    ),
    NavigationDestination(
      icon: Icon(Icons.compare_outlined),
      label: 'Stack vs column',
      selectedIcon: Icon(Icons.compare),
    ),
    NavigationDestination(
      icon: Icon(Icons.compare_arrows_outlined),
      label: 'State Events',
      selectedIcon: Icon(Icons.compare_arrows),
    ),
    NavigationDestination(
      icon: Icon(Icons.auto_mode_outlined),
      label: 'Theme picker',
      selectedIcon: Icon(Icons.auto_mode),
    ),
  ];

  @override
  Widget build(BuildContext context) => NavigationScaffold(
        appBar: AppBar(
          title: Text(_destinations[navigationShell.currentIndex].label),
        ),
        body: navigationShell,
        destinations: _destinations,
        scaffoldKey: BlocProvider.of<AppBloc>(context).scaffoldKey,
        selectedIndex: navigationShell.currentIndex,
        onDestinationSelected: navigationShell.goBranch,
      );
}
