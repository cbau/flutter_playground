import 'package:flutter/material.dart';

class NumberPicker extends StatelessWidget {
  const NumberPicker({
    this.controller,
    this.decoration = const InputDecoration(),
    this.keyboardType = TextInputType.number,
    this.readOnly = false,
    this.onAddPressed,
    this.onRemovePressed,
    this.textAlign = TextAlign.center,
    super.key,
  });

  final TextEditingController? controller;
  final InputDecoration decoration;
  final TextInputType keyboardType;
  final void Function()? onAddPressed;
  final void Function()? onRemovePressed;
  final bool readOnly;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) => TextField(
        controller: controller,
        decoration: decoration.copyWith(
          prefixIcon: IconButton(
            onPressed: onRemovePressed,
            icon: const Icon(Icons.remove),
          ),
          suffixIcon: IconButton(
            onPressed: onAddPressed,
            icon: const Icon(Icons.add),
          ),
        ),
        keyboardType: keyboardType,
        readOnly: readOnly,
        textAlign: textAlign,
      );
}
