import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/repositories/country_repository.dart';
import 'extended_widgets_state.dart';

class ExtendedWidgetsBloc extends Cubit<ExtendedWidgetsState> {
  ExtendedWidgetsBloc() : super(const ExtendedWidgetsState());

  final List<String> countries = CountryRepository().all;

  DateTime currentDateTime = DateTime.now();

  final dateTimePickerController = TextEditingController();

  final numberPickerController = TextEditingController(
    text: '0',
  );

  void onDateTimeSelected([DateTime? value]) {
    if (value != null) {
      currentDateTime = value;
      dateTimePickerController.text = currentDateTime.toString();
    }
  }

  void onNumberPickerAddPressed() {
    final currentValue = int.tryParse(numberPickerController.text) ?? 0;
    numberPickerController.text = '${currentValue + 1}';
  }

  void onNumberPickerRemovePressed() {
    final currentValue = int.tryParse(numberPickerController.text) ?? 0;
    numberPickerController.text = '${currentValue - 1}';
  }
}
