import 'package:flutter/material.dart';

class DateTimePicker extends StatelessWidget {
  const DateTimePicker({
    required this.firstDate,
    required this.initialDateTime,
    required this.lastDate,
    this.controller,
    this.decoration,
    this.readOnly = false,
    this.onDateTimeSelected,
    super.key,
  });

  final TextEditingController? controller;
  final InputDecoration? decoration;
  final DateTime firstDate;
  final DateTime initialDateTime;
  final DateTime lastDate;
  final void Function(DateTime? value)? onDateTimeSelected;
  final bool readOnly;

  @override
  Widget build(BuildContext context) => TextField(
        controller: controller,
        decoration: decoration,
        onTap: () async {
          var dateTime = await showDatePicker(
            context: context,
            firstDate: firstDate,
            initialDate: initialDateTime,
            lastDate: lastDate,
          );
          if (dateTime != null && context.mounted) {
            final time = await showTimePicker(
              context: context,
              initialTime: TimeOfDay.fromDateTime(initialDateTime),
            );
            if (time != null) {
              dateTime = dateTime.add(
                Duration(
                  hours: time.hour,
                  minutes: time.minute,
                ),
              );
              onDateTimeSelected?.call(dateTime);
            }
          }
        },
        readOnly: readOnly,
      );
}
