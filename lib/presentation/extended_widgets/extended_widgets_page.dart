import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/info_card.dart';
import 'date_time_picker.dart';
import 'extended_widgets_bloc.dart';
import 'extended_widgets_state.dart';
import 'number_picker.dart';

class ExtendedWidgetsPage extends StatelessWidget {
  ExtendedWidgetsPage({super.key});

  static const routeName = 'ExtendedWidgetsPage';

  final bloc = ExtendedWidgetsBloc();

  @override
  Widget build(BuildContext context) => Scaffold(
        body: BlocBuilder<ExtendedWidgetsBloc, ExtendedWidgetsState>(
          bloc: bloc,
          builder: (context, state) => ListView(
            padding: const EdgeInsets.all(16),
            children: [
              const InfoCard(
                title: 'Extended widgets',
                description: '''
  Have you ever though that there are some missing widgets in material app? Number pickers, DateTime pickers, you name it!
  Well, maybe you don't need new widgets for them, only knowing how to use your current tools to make them work.
  Here you can see some examples on how to achieve the "missing" widgets for your apps.''',
              ),
              const SizedBox(
                height: 8,
              ),
              Center(
                child: Link(
                  uri: Uri.parse(
                    '${AppService().baseUrl}/lib/presentation/extended_widgets/',
                  ),
                  builder: (context, followLink) => ElevatedButton.icon(
                    icon: const Icon(Icons.link),
                    label: const Text('Open the source code for this example'),
                    onPressed: followLink,
                  ),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              DropdownMenu<String>(
                dropdownMenuEntries: bloc.countries
                    .map(
                      (e) => DropdownMenuEntry(
                        label: e,
                        value: e,
                      ),
                    )
                    .toList(),
                enableFilter: true,
                enableSearch: false,
                expandedInsets: EdgeInsets.zero,
                label: const Text('Autocomplete'),
                menuStyle: const MenuStyle(
                  maximumSize: WidgetStatePropertyAll(Size.fromHeight(200)),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              DateTimePicker(
                controller: bloc.dateTimePickerController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  label: Text('Date and time picker'),
                ),
                firstDate: DateTime.now(),
                initialDateTime: bloc.currentDateTime,
                lastDate: DateTime.now().add(
                  const Duration(days: 365),
                ),
                onDateTimeSelected: bloc.onDateTimeSelected,
              ),
              const SizedBox(
                height: 16,
              ),
              NumberPicker(
                controller: bloc.numberPickerController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  label: Text('Number picker'),
                ),
                onAddPressed: bloc.onNumberPickerAddPressed,
                onRemovePressed: bloc.onNumberPickerRemovePressed,
              ),
            ],
          ),
        ),
      );
}
