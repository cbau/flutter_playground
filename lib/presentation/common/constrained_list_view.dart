import 'package:flutter/material.dart';

class ConstrainedListView extends StatelessWidget {
  const ConstrainedListView({
    required this.children,
    required this.constraints,
    this.alignment = Alignment.center,
    this.padding,
    super.key,
  });

  final AlignmentGeometry alignment;
  final List<Widget?> children;
  final BoxConstraints constraints;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) => ListView.builder(
        itemBuilder: (context, index) => Align(
          alignment: alignment,
          child: ConstrainedBox(
            constraints: constraints,
            child: children[index],
          ),
        ),
        itemCount: children.length,
        padding: padding,
      );
}
