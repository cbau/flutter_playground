import 'package:flutter/material.dart';

class GenericNavigationBar extends StatelessWidget {
  const GenericNavigationBar({super.key});

  @override
  Widget build(BuildContext context) => NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          NavigationDestination(
            icon: Icon(Icons.map),
            label: 'Map',
          ),
          NavigationDestination(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
      );
}
