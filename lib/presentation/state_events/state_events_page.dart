import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/info_card.dart';

class StateEventsPage extends StatelessWidget {
  const StateEventsPage({super.key});

  static const routeName = 'StateEventsPage';

  @override
  Widget build(BuildContext context) => Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const InfoCard(
              title: 'State Events',
              description: '''
Through the life cycle of each widget, many events happen one after the other. You can see the logs here to understand how and when they happen.''',
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: ElevatedButton.icon(
                icon: const Icon(Icons.arrow_forward),
                label: const Text('Open the events log'),
                onPressed: () async =>
                    GoRouter.of(context).goNamed(StateEventsLogPage.routeName),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(
                  '${AppService().baseUrl}/lib/presentation/state_events/state_events_page.dart',
                ),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the source code for this example'),
                  onPressed: followLink,
                ),
              ),
            ),
          ],
        ),
      );
}

class StateEventsLogPage extends StatefulWidget {
  const StateEventsLogPage({super.key});

  static const routeName = 'StateEventsLogPage';

  @override
  State<StateEventsLogPage> createState() => _StateEventsLogPageState();
}

class _StateEventsLogPageState extends State<StateEventsLogPage>
    with WidgetsBindingObserver {
  final _logs = <String>[];

  @override
  void activate() {
    super.activate();
    _printMessageWithTimestamp('Widget activated');
  }

  @override
  void deactivate() {
    super.deactivate();
    _printMessageWithTimestamp('Widget deactivated', skipScreenLog: true);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    _printMessageForAppLifecycleState(state);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _printMessageWithTimestamp('Widget dependencies changed');
  }

  @override
  void didUpdateWidget(covariant StateEventsLogPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    _printMessageWithTimestamp('Widget widget updated');
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _printMessageWithTimestamp('Widget disposed', skipScreenLog: true);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _printMessageWithTimestamp('Widget initialized');
    WidgetsBinding.instance.addObserver(this);
    _setInitialState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('State Events Log'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: _logs.map(Text.new).toList(),
        ),
      );

  void _printMessageForAppLifecycleState(AppLifecycleState state) {
    final message = switch (state) {
      AppLifecycleState.detached => 'App Lifecycle detached',
      AppLifecycleState.hidden => 'App Lifecycle hidden',
      AppLifecycleState.inactive => 'App Lifecycle inactive',
      AppLifecycleState.paused => 'App Lifecycle paused',
      AppLifecycleState.resumed => 'App Lifecycle resumed',
    };
    _printMessageWithTimestamp(message);
  }

  void _printMessageWithTimestamp(
    String message, {
    bool skipScreenLog = false,
  }) {
    final toPrint = '[${DateTime.now()}] $message';
    debugPrint(toPrint);
    if (!skipScreenLog) {
      setState(() => _logs.add(toPrint));
    }
  }

  void _setInitialState() {
    if (WidgetsBinding.instance.lifecycleState != null) {
      _printMessageForAppLifecycleState(
        WidgetsBinding.instance.lifecycleState!,
      );
    }
  }
}
