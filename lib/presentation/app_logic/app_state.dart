import 'package:flutter/material.dart';

class AppState {
  const AppState({
    this.color = Colors.blue,
    this.themeMode = ThemeMode.system,
  });

  final Color color;
  final ThemeMode themeMode;
}
