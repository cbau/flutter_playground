import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app_state.dart';

class AppBloc extends Cubit<AppState> {
  AppBloc() : super(const AppState());

  final scaffoldKey = GlobalKey<ScaffoldState>();

  Color _color = Colors.blue;
  ThemeMode _themeMode = ThemeMode.system;

  void setThemeMode(ThemeMode? value) {
    if (value != null) {
      _themeMode = value;
      emit(
        AppState(
          color: _color,
          themeMode: _themeMode,
        ),
      );
    }
  }

  void setColor(Color? value) {
    if (value != null) {
      _color = value;
      emit(
        AppState(
          color: _color,
          themeMode: _themeMode,
        ),
      );
    }
  }
}
