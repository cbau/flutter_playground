import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/info_card.dart';
import 'nested_pickers_bloc.dart';
import 'nested_pickers_state.dart';

class NestedPickersPage extends StatelessWidget {
  const NestedPickersPage({super.key});

  static const routeName = 'NestedPickersPage';

  @override
  Widget build(BuildContext context) {
    final bloc = NestedPickersBloc();
    return Scaffold(
      body: BlocBuilder<NestedPickersBloc, NestedPickersState>(
        bloc: bloc,
        builder: (context, state) => ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const InfoCard(
              title: 'Nested pickers',
              description: '''
  Simpler than it looks! A second picker whose content depends on a first picker.
  You just fill the content with the subvalues of the first picker, and let Flutter's widget rebuild make it's magic.
  This example is done with StatelessWidget, but the logic is the same for any state manager of your preference.''',
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(
                  '${AppService().baseUrl}/lib/presentation/nested_pickers/nested_pickers_page.dart',
                ),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the source code for this example'),
                  onPressed: followLink,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            DropdownMenu<int>(
              dropdownMenuEntries: state.items.indexed.map((e) {
                final (index, category) = e;
                return DropdownMenuEntry<int>(
                  value: index,
                  label: category.name,
                );
              }).toList(),
              expandedInsets: EdgeInsets.zero,
              initialSelection: state.selectedCategory,
              label: const Text('Platform'),
              onSelected: bloc.onCategoryChanged,
              requestFocusOnTap: false,
            ),
            const SizedBox(
              height: 16,
            ),
            DropdownMenu<int>(
              controller: bloc.optionController,
              dropdownMenuEntries:
                  state.items[state.selectedCategory].options.indexed.map((e) {
                final (index, name) = e;
                return DropdownMenuEntry<int>(
                  value: index,
                  label: name,
                );
              }).toList(),
              expandedInsets: EdgeInsets.zero,
              initialSelection: state.selectedOption,
              label: const Text('Target'),
              onSelected: bloc.onOptionChanged,
              requestFocusOnTap: false,
            ),
          ],
        ),
      ),
    );
  }
}
