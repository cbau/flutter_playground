import '../../data/models/category.dart';

class NestedPickersState {
  const NestedPickersState({
    this.items = const [],
    this.selectedCategory = 0,
    this.selectedOption = 0,
  });

  final List<Category> items;
  final int selectedCategory;
  final int selectedOption;
}
