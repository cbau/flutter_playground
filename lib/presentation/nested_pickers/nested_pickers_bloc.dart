import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/category.dart';
import 'nested_pickers_state.dart';

class NestedPickersBloc extends Cubit<NestedPickersState> {
  NestedPickersBloc() : super(const NestedPickersState()) {
    _emitUpdate();
  }

  final _items = [
    const Category(
      name: 'Desktop',
      options: [
        'Linux',
        'Mac',
        'Windows',
      ],
    ),
    const Category(
      name: 'Mobile',
      options: [
        'Android',
        'iOS',
      ],
    ),
    const Category(
      name: 'Web',
      options: [
        'Chrome / Edge',
        'Firefox',
        'Safari',
      ],
    ),
  ];
  final optionController = TextEditingController();

  var _selectedCategory = 0;
  var _selectedOption = 0;

  void _emitUpdate() {
    emit(
      NestedPickersState(
        items: _items,
        selectedCategory: _selectedCategory,
        selectedOption: _selectedOption,
      ),
    );
  }

  void onCategoryChanged(int? value) {
    if (value != null) {
      _selectedCategory = value;
      _selectedOption = 0;
      optionController.text = _items[value].options[0];
      _emitUpdate();
    }
  }

  void onOptionChanged(int? value) {
    if (value != null) {
      _selectedOption = value;
      _emitUpdate();
    }
  }
}
