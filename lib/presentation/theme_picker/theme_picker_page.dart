import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../app_logic/app_bloc.dart';
import '../app_logic/app_state.dart';
import '../common/info_card.dart';

class ThemePickerPage extends StatelessWidget {
  const ThemePickerPage({super.key});

  static const routeName = 'ThemePickerPage';

  @override
  Widget build(BuildContext context) => Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const InfoCard(
              title: 'Theme picker',
              description: '''
Light mode or dark mode? This picker will let the user choose which theme to use for this application, light, dark, or use the system theme, which is the default option.''',
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(
                  '${AppService().baseUrl}//lib/presentation/theme_picker/theme_picker_page.dart',
                ),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the source code for this example'),
                  onPressed: followLink,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            BlocBuilder<AppBloc, AppState>(
              builder: (context, state) => Column(
                children: [
                  DropdownMenu<ThemeMode>(
                    dropdownMenuEntries: const [
                      DropdownMenuEntry<ThemeMode>(
                        label: 'Automatic',
                        leadingIcon: Icon(Icons.auto_mode_outlined),
                        value: ThemeMode.system,
                      ),
                      DropdownMenuEntry<ThemeMode>(
                        label: 'Light',
                        leadingIcon: Icon(Icons.light_mode_outlined),
                        value: ThemeMode.light,
                      ),
                      DropdownMenuEntry<ThemeMode>(
                        label: 'Dark',
                        leadingIcon: Icon(Icons.dark_mode_outlined),
                        value: ThemeMode.dark,
                      ),
                    ],
                    expandedInsets: EdgeInsets.zero,
                    initialSelection: state.themeMode,
                    label: const Text('Theme'),
                    onSelected: BlocProvider.of<AppBloc>(context).setThemeMode,
                    requestFocusOnTap: false,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  DropdownMenu<Color>(
                    dropdownMenuEntries: const [
                      DropdownMenuEntry<Color>(
                        label: 'Blue',
                        leadingIcon: _CircleBox(
                          color: Colors.blue,
                          dimension: 24,
                        ),
                        value: Colors.blue,
                      ),
                      DropdownMenuEntry<Color>(
                        label: 'Green',
                        leadingIcon: _CircleBox(
                          color: Colors.green,
                          dimension: 24,
                        ),
                        value: Colors.green,
                      ),
                      DropdownMenuEntry<Color>(
                        label: 'Red',
                        leadingIcon: _CircleBox(
                          color: Colors.red,
                          dimension: 24,
                        ),
                        value: Colors.red,
                      ),
                      DropdownMenuEntry<Color>(
                        label: 'Yellow',
                        leadingIcon: _CircleBox(
                          color: Colors.yellow,
                          dimension: 24,
                        ),
                        value: Colors.yellow,
                      ),
                    ],
                    expandedInsets: EdgeInsets.zero,
                    initialSelection: state.color,
                    label: const Text('Color'),
                    onSelected: BlocProvider.of<AppBloc>(context).setColor,
                    requestFocusOnTap: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}

class _CircleBox extends StatelessWidget {
  const _CircleBox({
    this.color,
    this.dimension,
  });

  final Color? color;
  final double? dimension;

  @override
  Widget build(BuildContext context) => SizedBox.square(
        dimension: dimension,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: color,
            shape: BoxShape.circle,
          ),
        ),
      );
}
