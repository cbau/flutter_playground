import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/favorite_card.dart';
import '../common/info_card.dart';

class HorizontalInVerticalListPage extends StatelessWidget {
  const HorizontalInVerticalListPage({super.key});

  static const routeName = 'HorizontalInVerticalListPage';

  @override
  Widget build(BuildContext context) => Scaffold(
        body: ListView(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: InfoCard(
                title: 'Horizontal list inside a vertical list',
                description: '''
Can you use a horizontal list inside a vertical list?
Yes you can! But you can only scroll in one direction at the time, and the scroll you want to do may not always respond as you expect.
Also, don't forget that you will need a touch screen or a 2D trackpad/trackball to move horizontally by default. To support a horizontal scroll for one dimentional trackballs (most standard mouses), you will need to use a Scrollbar widget as well.''',
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(
                  '${AppService().baseUrl}/lib/presentation/horizontal_in_vertical_list/horizontal_in_vertical_list_page.dart',
                ),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the source code for this example'),
                  onPressed: followLink,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            _HorizontalList(),
            const ListTile(
              title: Text('Lorem'),
            ),
            const ListTile(
              title: Text('Ipsum'),
            ),
            const ListTile(
              title: Text('Dolor'),
            ),
            const ListTile(
              title: Text('Sit'),
            ),
            const ListTile(
              title: Text('Amet'),
            ),
            const ListTile(
              title: Text('Lorem'),
            ),
            const ListTile(
              title: Text('Ipsum'),
            ),
            const ListTile(
              title: Text('Dolor'),
            ),
            const ListTile(
              title: Text('Sit'),
            ),
            const ListTile(
              title: Text('Amet'),
            ),
          ],
        ),
      );
}

class _HorizontalList extends StatelessWidget {
  _HorizontalList();

  final _horizontalScrollController = ScrollController();

  @override
  Widget build(BuildContext context) => SizedBox(
        height: 240,
        child: Scrollbar(
          controller: _horizontalScrollController,
          child: ListView.builder(
            controller: _horizontalScrollController,
            itemCount: 6,
            itemBuilder: (context, index) => SizedBox(
              width: 320,
              child: FavoriteCard(
                isFavorite: index.isOdd,
                title: 'TITLE ${index + 1}',
                description: 'SUBTITLE',
              ),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 8,
            ),
            scrollDirection: Axis.horizontal,
          ),
        ),
      );
}
