import 'package:flutter/material.dart';

class AppTheme {
  factory AppTheme() => _instance;

  AppTheme._();

  static final AppTheme _instance = AppTheme._();

  ThemeData build({required bool isLight, Color? color}) => ThemeData(
        brightness: isLight ? Brightness.light : Brightness.dark,
        colorSchemeSeed: color ?? Colors.blue,
      );
}
