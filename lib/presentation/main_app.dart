import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app_logic/app_bloc.dart';
import 'app_logic/app_state.dart';
import 'app_router.dart';
import 'app_theme.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => AppBloc()),
        ],
        child: BlocBuilder<AppBloc, AppState>(
          builder: (context, state) => MaterialApp.router(
            darkTheme: AppTheme().build(
              color: state.color,
              isLight: false,
            ),
            routerConfig: AppRouter().router,
            theme: AppTheme().build(
              color: state.color,
              isLight: true,
            ),
            themeMode: state.themeMode,
            title: 'Flutter Playground',
          ),
        ),
      );
}
