import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/generic_navigation_bar.dart';
import '../common/info_card.dart';

class StackVsColumnPage extends StatelessWidget {
  const StackVsColumnPage({super.key});

  static const routeName = 'StackVsColumnPage';

  @override
  Widget build(BuildContext context) => const DefaultTabController(
        length: 3,
        child: Scaffold(
          body: Column(
            children: [
              TabBar(
                tabs: [
                  Tab(
                    text: 'Stack',
                  ),
                  Tab(
                    text: 'Column',
                  ),
                  Tab(
                    icon: Icon(Icons.info_outline),
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  children: [
                    _StackContainer(),
                    _ColumnContainer(),
                    _InfoContainer(),
                  ],
                ),
              ),
            ],
          ),
          bottomNavigationBar: GenericNavigationBar(),
        ),
      );
}

class _StackContainer extends StatelessWidget {
  const _StackContainer();

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          Center(
            child: _ErrorText(),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: _RetryButton(),
          ),
        ],
      );
}

class _ColumnContainer extends StatelessWidget {
  const _ColumnContainer();

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Expanded(
            child: Center(
              child: _ErrorText(),
            ),
          ),
          _RetryButton(),
        ],
      );
}

class _ErrorText extends StatelessWidget {
  @override
  Widget build(BuildContext context) => const Text(
        'This simulates a dynamic error coming from an unsuccessful request.',
        textAlign: TextAlign.center,
      );
}

class _RetryButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(8),
        child: OutlinedButton.icon(
          onPressed: () {},
          icon: const Icon(Icons.replay),
          label: const Text('Retry'),
        ),
      );
}

class _InfoContainer extends StatelessWidget {
  const _InfoContainer();

  @override
  Widget build(BuildContext context) => ListView(
        padding: const EdgeInsets.all(16),
        children: [
          const InfoCard(
            title: 'Stack vs column',
            description: '''
So you want a centered content in your almost empty page? You could either use a Stack, or a Column.
Using Stack will center your content from the whole available height. But make sure that your centered content will be smaller than the surrounding content, or you have the risk to overlap it. This usually happens when you load your content dinamically, and you have no control over it.
Using Column on the other hand, will center the content with the remaining space left by the other elements. This also means that the centered content won't overlap with the other elements if the content results bigger than expected.
So, which one will you choose to use? Stack, or Column?''',
          ),
          const SizedBox(
            height: 8,
          ),
          Center(
            child: Link(
              uri: Uri.parse(
                '${AppService().baseUrl}/lib/presentation/stack_vs_column/stack_vs_column_page.dart',
              ),
              builder: (context, followLink) => ElevatedButton.icon(
                icon: const Icon(Icons.link),
                label: const Text(
                  'Open the source code for this example',
                ),
                onPressed: followLink,
              ),
            ),
          ),
        ],
      );
}
