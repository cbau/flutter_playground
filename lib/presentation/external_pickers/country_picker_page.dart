import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'country_picker_bloc.dart';
import 'country_picker_state.dart';

class CountryPickerPage extends StatelessWidget {
  CountryPickerPage({super.key});

  static const routeName = 'CountryPickerPage';

  final bloc = CountryPickerBloc();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Select a country'),
        ),
        body: BlocBuilder<CountryPickerBloc, CountryPickerState>(
          bloc: bloc,
          builder: (context, state) => Column(
            children: [
              TextField(
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  label: Text('Search'),
                ),
                onChanged: bloc.onFilterChanged,
              ),
              Expanded(
                child: ListView.builder(
                  itemBuilder: (context, index) => ListTile(
                    onTap: () =>
                        Navigator.of(context).pop(state.countries[index]),
                    title: Text(state.countries[index]),
                  ),
                  itemCount: state.countries.length,
                ),
              ),
            ],
          ),
        ),
      );
}
