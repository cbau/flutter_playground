import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/info_card.dart';
import 'country_picker_page.dart';
import 'genre_picker_dialog.dart';

class ExternalPickersPage extends StatelessWidget {
  ExternalPickersPage({super.key});

  static const routeName = 'ExternalPickersPage';

  final countryController = TextEditingController();
  final genreController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const InfoCard(
              title: 'External pickers',
              description: '''
  External pickers open a new widget to receive the user selection.
  Dialog pickers will open a dialog above the current screen with the picker options. This is a good alternative for pickers with a few options.
  Page pickers will open a new page with the picker options. This is a great alternative for pickers with many options, and allow more advanced controls like a filtering text field.''',
            ),
            const SizedBox(
              height: 8,
            ),
            Center(
              child: Link(
                uri: Uri.parse(
                  '${AppService().baseUrl}/lib/presentation/external_pickers/',
                ),
                builder: (context, followLink) => ElevatedButton.icon(
                  icon: const Icon(Icons.link),
                  label: const Text('Open the source code for this example'),
                  onPressed: followLink,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            _PickerTextField(
              controller: genreController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Genre'),
              ),
              onTap: () async {
                final selectedOption = await showDialog<String>(
                  context: context,
                  builder: (context) => const GenrePickerDialog(),
                );
                if (selectedOption != null) {
                  genreController.text = selectedOption;
                }
              },
            ),
            const SizedBox(
              height: 8,
            ),
            _PickerTextField(
              controller: countryController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Country'),
              ),
              onTap: () async {
                final selectedOption = await GoRouter.of(context)
                    .pushNamed<String>(CountryPickerPage.routeName);
                if (selectedOption != null) {
                  countryController.text = selectedOption;
                }
              },
            ),
          ],
        ),
      );
}

class _PickerTextField extends StatelessWidget {
  const _PickerTextField({
    this.controller,
    this.decoration,
    this.onTap,
  });

  final TextEditingController? controller;
  final InputDecoration? decoration;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) => TextField(
        canRequestFocus: false,
        controller: controller,
        decoration: (decoration ?? const InputDecoration()).copyWith(
          suffixIcon: IconButton(
            onPressed: onTap,
            icon: const Icon(Icons.arrow_drop_down),
          ),
        ),
        onTap: onTap,
      );
}
