class CountryPickerState {
  const CountryPickerState({
    this.countries = const [],
  });

  final List<String> countries;
}
