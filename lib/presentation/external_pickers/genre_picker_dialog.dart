import 'package:flutter/material.dart';

class GenrePickerDialog extends StatelessWidget {
  const GenrePickerDialog({super.key});

  @override
  Widget build(BuildContext context) => Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: const Icon(Icons.man),
              onTap: () => Navigator.of(context).pop('Male'),
              title: const Text('Male'),
            ),
            ListTile(
              leading: const Icon(Icons.woman),
              onTap: () => Navigator.of(context).pop('Female'),
              title: const Text('Female'),
            ),
            ListTile(
              leading: const Icon(Icons.wc),
              onTap: () => Navigator.of(context).pop('Other'),
              title: const Text('Other'),
            ),
          ],
        ),
      );
}
