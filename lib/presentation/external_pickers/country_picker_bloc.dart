import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/repositories/country_repository.dart';
import 'country_picker_state.dart';

class CountryPickerBloc extends Cubit<CountryPickerState> {
  CountryPickerBloc() : super(const CountryPickerState()) {
    onFilterChanged();
  }

  final List<String> allCountries = CountryRepository().all;

  void onFilterChanged([String? value]) {
    if (value?.isEmpty ?? true) {
      emit(
        CountryPickerState(
          countries: allCountries,
        ),
      );
    } else {
      final filteredCountries = allCountries
          .where(
            (country) => country.toLowerCase().contains(value!.toLowerCase()),
          )
          .toList();
      emit(
        CountryPickerState(
          countries: filteredCountries,
        ),
      );
    }
  }
}
