import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';

import 'about/about_page.dart';
import 'common/not_found_page.dart';
import 'extended_widgets/extended_widgets_page.dart';
import 'external_pickers/country_picker_page.dart';
import 'external_pickers/external_pickers_page.dart';
import 'grouped_list/grouped_list_page.dart';
import 'horizontal_in_vertical_list/horizontal_in_vertical_list_page.dart';
import 'navigation/navigation_page.dart';
import 'navigation/responsive_navigation_page.dart';
import 'nested_pickers/nested_pickers_page.dart';
import 'responsive_form/responsive_form_page.dart';
import 'stack_vs_column/stack_vs_column_page.dart';
import 'state_events/state_events_page.dart';
import 'theme_picker/theme_picker_page.dart';

class AppRouter {
  factory AppRouter() => _instance;

  AppRouter._();

  static final AppRouter _instance = AppRouter._();

  final _rootNavigatorKey = GlobalKey<NavigatorState>();

  late final router = GoRouter(
    errorBuilder: (context, state) => const NotFoundPage(),
    navigatorKey: _rootNavigatorKey,
    routes: [
      StatefulShellRoute.indexedStack(
        builder: (context, state, navigationShell) => NavigationPage(
          navigationShell: navigationShell,
        ),
        branches: [
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const AboutPage(),
                name: AboutPage.routeName,
                path: '/',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => ExtendedWidgetsPage(),
                name: ExtendedWidgetsPage.routeName,
                path: '/extended-widgets',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => ExternalPickersPage(),
                name: ExternalPickersPage.routeName,
                path: '/external-pickers',
                routes: [
                  GoRoute(
                    builder: (context, state) => CountryPickerPage(),
                    name: CountryPickerPage.routeName,
                    parentNavigatorKey: _rootNavigatorKey,
                    path: 'countries',
                  ),
                ],
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const GroupedListPage(),
                name: GroupedListPage.routeName,
                path: '/grouped-list',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) =>
                    const HorizontalInVerticalListPage(),
                name: HorizontalInVerticalListPage.routeName,
                path: '/horizontal-in-vertical-list',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const NestedPickersPage(),
                name: NestedPickersPage.routeName,
                path: '/nested-pickers',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const ResponsiveFormPage(),
                name: ResponsiveFormPage.routeName,
                path: '/responsive-form',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const ResponsiveNavigationPage(),
                name: ResponsiveNavigationPage.routeName,
                path: '/responsive-navigation',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const StackVsColumnPage(),
                name: StackVsColumnPage.routeName,
                path: '/stack-vs-column',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const StateEventsPage(),
                name: StateEventsPage.routeName,
                path: '/state-events',
                routes: [
                  GoRoute(
                    builder: (context, state) => const StateEventsLogPage(),
                    name: StateEventsLogPage.routeName,
                    parentNavigatorKey: _rootNavigatorKey,
                    path: 'log',
                  ),
                ],
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                builder: (context, state) => const ThemePickerPage(),
                name: ThemePickerPage.routeName,
                path: '/theme-picker',
              ),
            ],
          ),
        ],
      ),
    ],
  );
}
