import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/constrained_list_view.dart';
import '../common/info_card.dart';

class ResponsiveFormPage extends StatelessWidget {
  const ResponsiveFormPage({super.key});

  static const routeName = 'ResponsiveFormPage';

  @override
  Widget build(BuildContext context) {
    final flexDirection = MediaQuery.of(context).size.width < 400
        ? Axis.vertical
        : Axis.horizontal;

    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ConstrainedListView(
              constraints: const BoxConstraints(
                maxWidth: 800,
              ),
              padding: const EdgeInsets.all(16),
              children: [
                const InfoCard(
                  title: 'Responsive form',
                  description: '''
A form with a max width for big screens, that changes from two columns to just one on small screens.
The secret sauce, is the use of Flex.''',
                ),
                const SizedBox(
                  height: 8,
                ),
                Center(
                  child: Link(
                    uri: Uri.parse(
                      '${AppService().baseUrl}/lib/presentation/responsive_form/responsive_form_page.dart',
                    ),
                    builder: (context, followLink) => ElevatedButton.icon(
                      icon: const Icon(Icons.link),
                      label:
                          const Text('Open the source code for this example'),
                      onPressed: followLink,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                  width: 8,
                ),
                Flex(
                  mainAxisSize: MainAxisSize.min,
                  direction: flexDirection,
                  children: const [
                    _TextFormFieldContainer(
                      label: Text('Given name'),
                    ),
                    SizedBox(
                      height: 8,
                      width: 8,
                    ),
                    _TextFormFieldContainer(
                      label: Text('Family name'),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                  width: 8,
                ),
                Flex(
                  direction: flexDirection,
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    _TextFormFieldContainer(
                      label: Text('City'),
                    ),
                    SizedBox(
                      height: 8,
                      width: 8,
                    ),
                    _TextFormFieldContainer(
                      label: Text('Country'),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                  width: 8,
                ),
                Flex(
                  direction: flexDirection,
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    _TextFormFieldContainer(
                      label: Text('E-mail'),
                    ),
                    SizedBox(
                      height: 8,
                      width: 8,
                    ),
                    _TextFormFieldContainer(
                      label: Text('Phone number'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 800,
            ),
            child: Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: ElevatedButton(
                  onPressed: () {},
                  child: const Text('Submit'),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _TextFormFieldContainer extends StatelessWidget {
  const _TextFormFieldContainer({
    this.label,
  });

  final Widget? label;

  @override
  Widget build(BuildContext context) => Flexible(
        child: TextFormField(
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            label: label,
          ),
        ),
      );
}
