import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:url_launcher/link.dart';

import '../../data/services/app_service.dart';
import '../common/info_card.dart';

class GroupedListPage extends StatelessWidget {
  const GroupedListPage({super.key});

  static const routeName = 'GroupedListPage';

  @override
  Widget build(BuildContext context) => Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  children: [
                    const InfoCard(
                      title: 'Grouped list',
                      description: '''
  Grouped lists with sticky headers, made with the magic of SliverList.
  The original plan was to use SliverPersistentHeader, but the headers went behind each other instead of above, not letting the right section to be shown at all time. And any possible solution seemed to complex to implement quicky.
  In the end, we end up using the package `flutter_sticky_header`. Although not using packages is the best option most of the times, cases like this show us that some packages are a better option than reinventing the wheel.''',
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Center(
                      child: Link(
                        uri: Uri.parse(
                          '${AppService().baseUrl}/lib/presentation/grouped_list/grouped_list_page.dart',
                        ),
                        builder: (context, followLink) => ElevatedButton.icon(
                          icon: const Icon(Icons.link),
                          label: const Text(
                            'Open the source code for this example',
                          ),
                          onPressed: followLink,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
            ),
            SliverStickyHeader(
              header: ColoredBox(
                color: Theme.of(context).colorScheme.surfaceContainer,
                child: const Padding(
                  padding: EdgeInsets.all(16),
                  child: Text('Section 1'),
                ),
              ),
              sliver: SliverList.builder(
                itemBuilder: (context, index) => ListTile(
                  title: Text('Item 1.${index + 1}'),
                ),
                itemCount: 10,
              ),
            ),
            SliverStickyHeader(
              header: ColoredBox(
                color: Theme.of(context).colorScheme.surfaceContainer,
                child: const Padding(
                  padding: EdgeInsets.all(16),
                  child: Text('Section 2'),
                ),
              ),
              sliver: SliverList.builder(
                itemBuilder: (context, index) => ListTile(
                  title: Text('Item 2.${index + 1}'),
                ),
                itemCount: 10,
              ),
            ),
            SliverStickyHeader(
              header: ColoredBox(
                color: Theme.of(context).colorScheme.surfaceContainer,
                child: const Padding(
                  padding: EdgeInsets.all(16),
                  child: Text('Section 3'),
                ),
              ),
              sliver: SliverList.builder(
                itemBuilder: (context, index) => ListTile(
                  title: Text('Item 3.${index + 1}'),
                ),
                itemCount: 10,
              ),
            ),
          ],
        ),
      );
}

/*
class _SliverStickyHeader extends StatelessWidget {
  const _SliverStickyHeader({
    this.child,
  });

  final Widget? child;

  @override
  Widget build(BuildContext context) => SliverPersistentHeader(
        delegate: _SliverStickyHeaderDelegate(
          child: child,
        ),
        floating: true,
        pinned: true,
      );
}

class _SliverStickyHeaderDelegate extends SliverPersistentHeaderDelegate {
  const _SliverStickyHeaderDelegate({
    this.child,
  });

  final Widget? child;

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) =>
      ColoredBox(
        color: Theme.of(context).colorScheme.surfaceVariant,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
            ),
            child: child,
          ),
        ),
      );

  @override
  double get maxExtent => 48;

  @override
  double get minExtent => 48;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      false;
}
*/
