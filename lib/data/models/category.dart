class Category {
  const Category({
    required this.name,
    required this.options,
  });

  final String name;
  final List<String> options;
}
