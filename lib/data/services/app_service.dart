class AppService {
  factory AppService() => _instance;

  const AppService._();

  static const _instance = AppService._();

  String get baseUrl =>
      'https://gitlab.com/cbau/flutter_playground/-/tree/main';
}
